El presente documento tiene como objetivo dar una breve explicacion del contenido de los archivos del proyecto 
de desarrollo para que completen la primer parte de la materia, sobre lenguaje C.

Estructura gral de los fuentes, esta sobre carpeta src
en carpeta lib esta la implementacion del ORM creado para acceso a la base, tambien fuentes de funciones utiles.

En carpeta script esta el conjunto de sentencias para crear sobre base de datos "medic" para que se creen tablas y relaciones, e incluyan datos iniciales.

La estructura gral esta armada para se usada en entorno Devc++, el archivo MakeFile sin extension sirve para la compilacion en Linux.

Configuracion de Proyecto DevC++ para linkear las librerias libpq, se tiene que ir a opciones de proyecto, parametros(parameters) campo Linker, elegir desde el
path de instalacion de PostgreSQL, en archivos de programas, segun version "9.X" dentro de carpeta "lib" ubicar archivo "libpq.lib" o sino "libpq.a". 
Luego ir a solapa "directorios" -> "Includes", alli seleccionar desde la instalacion de PostgreSQL, la carpeta "Include, "ADD" o "Añadir" y click en Aceptar para confirmar el 
cambio.
