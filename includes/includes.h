#include <string.h>
#include <errno.h>
#include "../config/config.h"
#include "../lib/object/object.h"
#include "../lib/orm/orm.h"
#include "../lib/utils/utils.h"

#include "../src/tipo_diagnostico/tipo_diagnostico.h"
#include "../src/obra_social/obra_social.h"
#include "../src/especialidad/especialidad.h"
#include "../src/localidad/localidad.h"
#include "../src/consultorio/consultorio.h"
#include "../src/paciente/paciente.h"
#include "../src/profesional/profesional.h"
#include "../src/profesional_especialidad/profesional_especialidad.h"
#include "../src/historial_paciente/historial_paciente.h"
#include "../src/turno/turno.h"

