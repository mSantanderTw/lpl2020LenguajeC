# Project: tpfinalC2020
# Asignature: Laboratorio de Programación y Lenguajes
# Year: 2020

CC       = gcc 

OBJ      = main.o lib/orm/orm.o lib/object/object.o lib/utils/utils.o src/consultorio/consultorio.o src/especialidad/especialidad.o src/localidad/localidad.o src/tipo_diagnostico/tipo_diagnostico.o src/obra_social/obra_social.o src/paciente/paciente.o src/profesional/profesional.o src/profesional_especialidad/profesional_especialidad.o src/historial_paciente/historial_paciente.o src/turno/turno.o 

INCS     =  -I"/usr/include/postgresql/"

BIN      = tpfinal2020

CFLAGS   = $(INCS) -fpic -lpq -g -Wno-unused-variable -Wno-int-to-pointer-cast -Wno-format 

RM       = rm -f

.PHONY: all clean

all: $(BIN)

clean:
	${RM} $(OBJ) $(BIN)

$(BIN): $(OBJ)
	$(CC) $(OBJ) -o $(BIN) $(CFLAGS)

main.o: main.c
	$(CC) -c main.c -o main.o $(CFLAGS)

lib/object/object.o: lib/object/object.c
	$(CC) -c lib/object/object.c -o lib/object/object.o $(CFLAGS)

lib/orm/orm.o: lib/orm/orm.c
	$(CC) -c lib/orm/orm.c -o lib/orm/orm.o $(CFLAGS)

lib/utils/utils.o: lib/utils/utils.c
	$(CC) -c lib/utils/utils.c -o lib/utils/utils.o $(CFLAGS)

src/consultorio/consultorio.o: src/consultorio/consultorio.c
	$(CC) -c src/consultorio/consultorio.c -o src/consultorio/consultorio.o $(CFLAGS)

src/especialidad/especialidad.o: src/especialidad/especialidad.c
	$(CC) -c src/especialidad/especialidad.c -o src/especialidad/especialidad.o $(CFLAGS)

src/tipo_diagnostico/tipo_diagnostico.o: src/tipo_diagnostico/tipo_diagnostico.c
	$(CC) -c src/tipo_diagnostico/tipo_diagnostico.c -o src/tipo_diagnostico/tipo_diagnostico.o $(CFLAGS)

src/localidad/localidad.o: src/localidad/localidad.c
	$(CC) -c src/localidad/localidad.c -o src/localidad/localidad.o $(CFLAGS)

src/obra_social/obra_social.o: src/obra_social/obra_social.c
	$(CC) -c src/obra_social/obra_social.c -o src/obra_social/obra_social.o $(CFLAGS)

src/paciente/paciente.o: src/paciente/paciente.c
	$(CC) -c src/paciente/paciente.c -o src/paciente/paciente.o $(CFLAGS)

src/profesional/profesional.o: src/profesional/profesional.c
	$(CC) -c src/profesional/profesional.c -o src/profesional/profesional.o $(CFLAGS)

src/profesional_especialidad/profesional_especialidad.o: src/profesional_especialidad/profesional_especialidad.c
	$(CC) -c src/profesional_especialidad/profesional_especialidad.c -o src/profesional_especialidad/profesional_especialidad.o $(CFLAGS)

src/historial_paciente/historial_paciente.o: src/historial_paciente/historial_paciente.c
	$(CC) -c src/historial_paciente/historial_paciente.c -o src/historial_paciente/historial_paciente.o $(CFLAGS)

src/turno/turno.o: src/turno/turno.c
	$(CC) -c src/turno/turno.c -o src/turno/turno.o $(CFLAGS)
