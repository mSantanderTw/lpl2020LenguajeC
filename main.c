#include <stdio.h>
#include <stdlib.h>
#include <libpq-fe.h> 
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "includes/includes.h"

int main(int argc, char *argv[])
{	
  obj_Consultorio *cons;
  
  void *itm;
  int i=0,size=0;
  void *list;
  // ejemplo como recuperar un elemento por clave  
  cons = Consultorio_new(); // construir instancia a utilizar - Consultorio
  cons->findbykey(cons,1); //buscar por clave
  // realizar cambios en algunos datos  
  cons->setDomicilio(cons,"Pellegrini 62");
  //guardar informacion del pseudoObjeto
  cons->saveObj(cons);
  // ejemplo para listar toda la informacion de un determinado tipo: pseudoObjeto Cansultorio
  size = cons->findAll(cons,&list,NULL); // con parametro de criterio NULL, lista toda la tabla, el criterio es de tipo cadena
  //y se usa en base a las columnas de la tabla, se aplica a condicion Where.
  for(i=0;i<size;++i)
  {
    itm = ((obj_Consultorio **)list)[i];    
    ((obj_Consultorio *)itm)->toString(itm);
  }
  // librerar memoria
  destroyObjList(list,size);  // liberar listado, cada instacia creada en el listado
  destroyObj(cons); // liberar lo que se creo, liberar bien la memoria.....
  
  system("PAUSE");
  return 0;
}
