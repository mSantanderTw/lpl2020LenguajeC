#ifndef OBJ_INCLUDED
	#define OBJ_INCLUDED
	typedef enum {t_int=1,t_varchar,t_bool,t_float,t_date} tipo_dato;
	#define FRMTYPE(t) (t==t_int || t==t_bool ? "%d":(t==t_float?"%f":"'%s'"))
	typedef struct  {
		int pos;
		void *value;
	}t_value;
	//----------------------------------------------------
	//Formato de una columna: nombre, tipo, es primary key? tama�o para varchar
	typedef struct{
	  char nombre[MAX];
	  tipo_dato tipo;
	  size_t size;
	  bool pkey;
	  bool autonum;
	}t_column;
	//----------------------------------------------------
	typedef struct {
		char nombre[MAX]; // nombre de la tabla
		int cant_columns; //cantidad de columnas
	    int cant_rows; // cantidad de filas
	    void *columns; //coleccion de columnas
	    void *rows; //coleccion de filas
	}t_table;
	//definicion de tipos de punteros a funcion que puede tener un pseudoObjeto
		typedef int  (*findAllPtr)(void *self, void **list,char *criteria); 
		typedef void (*toStringPtr)(void *self);
		typedef bool (*getIsNewObjPtr)(void *self);
		typedef bool (*saveObjPtr)(void *self);
		typedef void (*destroyInternalPtr)(void *self);
		typedef int (*findbykeyPtr)(void *self,int id);
		typedef void (*procedimientoPtr)(void *self);
		
		// definicion para propiedades getter u setters.
		typedef int   (*getPropertyIntPtr)(void *self);
		typedef char *(*getPropertyCharPtr)(void *self);
		typedef void  (*setPropertyIntPtr)(void *self,int val);
		typedef void  (*setPropertyCharPtr)(void *self,char *val);
		typedef void  (*setIdPtr)(void *self,int val);
	//----------------------------------------------------
	#define TYPEDEFCOMMON t_table *ds; 			\
			t_value **col_value; 				\
			size_t sizeObj;      				\
			void *constructor;   				\
			findAllPtr findAll;  				\
			toStringPtr toString; 				\
			getIsNewObjPtr getIsNewObj; 		\
			saveObjPtr saveObj;           		\
			destroyInternalPtr destroyInternal; \
			bool isNewObj;                      \
			findbykeyPtr findbykey;				\
			setIdPtr setId;						
			
	// Estructura de pseudoobjeto gen�rico: Object 
	typedef struct 
	{
		TYPEDEFCOMMON
	}t_object;
	// manejo generico para buscar todos los elementos de una clase dada
	int findAllImpl(void *self,void **list, char *criteria);
	// manejo generico, implementacion de punteros a funcion en base a interface t_objeto *
	// definicion generica t_objeto* para guardar info 
	bool saveObj_Impl(void *self);
	// definicion generica t_objeto* para buscar info por un id(INT)
	int find_Impl(void *self, int k);
	// definicion generica t_objeto* para buscar info por criterio o Todo 
	int findAll_Impl(void *self,void **list, char *criteria);
	// manejo generico para obtener si pseudo objeto esta recientemente instanciado o recuperado desde la base de datos.
	bool getIsNewObj_Impl(void *self);
	// manejo para seteo Comun de Id de la clase
	void setId_Impl(void *self, int);
	// manejo generico para inicializar un pseudo objeto
	void *init_obj(size_t, void*(*)(void *));
	// manejo generico para listar
	void listObj(void *objList, char *criteria, int freeObj,void(*lstFunc)(void *o));
	// manejo generico para liberar memoria de un pseudo objeto
	void destroyObj(void *);
	// manejo generico para liberar recurso de un listado.
	void destroyObjList(void **,int);
		

	// MACRO DE REEMPLAZO DE IMPLEMENTACION DE INTERFAZ COMUN, SIMULA HERENCIA DE ALGUNOS PUNTEROS A FUNCION COMUNES. ACA SE PODRIA
	// AGREGAR NUEVA FUNCIONALIDAD COMUN, ES DONDE SE INDICA PUNTERO A FUNCION QUE SE DEFINE EN ORM.C, EL FORMATO DEL PUNTERO SE DEFINE MAS
	// ARRIBA EN MACRO TYPEDEFCOMMON
	#define INIT_COMMON   obj->getIsNewObj      = getIsNewObj_Impl; \
	  					  obj->findbykey        = find_Impl;		\
	  					  obj->findAll          = findAll_Impl;		\
	  					  obj->saveObj          = saveObj_Impl;		\
						  obj->setId			= setId_Impl; 					  

#endif
