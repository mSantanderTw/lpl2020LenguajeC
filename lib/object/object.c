#include "../../config/config.h"
#include "../utils/utils.h"
#include "object.h"
//----------------------------------------------------
// Implementacion comun para todos los objetos, interface T_OBJECT *, solo resta discriminar TOSTRING, por que cada pseudoOnjeto
// tendra su propia implementacion segun sus columnas
//----------------------------------------------------
bool getIsNewObj_Impl(void *self)
{
	return ((t_object *)self)->isNewObj;
}
//----------------------------------------------------
// implementacion para copiar toda la informacion segun un criterio ejecutado en la base de datos
 int findAll_Impl(void *self,void **list, char *criteria)
{
  return findAllImpl(self,list,criteria);
}
//----------------------------------------------------
// implementacion de metodos para Consultorio
 int find_Impl(void *self, int k) // se debe pasar en orden de aparicion de las columnas claves 
{
   int size=0; 
   t_object *obj = ((t_object *)self);
   // inicializar Id en columna clave (campo Id)
   obj->setId(obj,k);
   size = findImpl(self);
   if(!size) // en find by key rellenar obj que invoca, con tupla 0 si hay.
   	 size = NOT_FOUND;
	return size;
}
//----------------------------------------------------
//Guardar informacion de la especialidad, si es nueva instancia, realiza insert, sino update
 bool saveObj_Impl(void *self)
{
   t_object *obj = ((t_object *)self);
   int newId;
   bool isNewObj = obj->getIsNewObj(self);
   bool retValue = saveObjImpl(self,&newId);
   
   if(isNewObj)
     obj->setId(obj,newId);
   return retValue;
}
// FIN IMPLEMENTACION INTERFACE COMUN
//----------------------------------------------------
//----------------------------------------------------
void *init_obj(size_t t_obj,void*(*init)(void *o))
{
   	 // pedir memoria para el objeto gral
    void *obj = malloc(t_obj);
	((t_object*)obj)->destroyInternal=NULL;//poner puntero a funcion en NULL, la invocacion de init de cada pseudoobjeto debe crear su version de detroyInternal
    void *oobj = init(obj);
    int i;
    t_table *tt = ((t_object*)oobj)->ds;
    ((t_object*)oobj)->col_value =  (t_value **)malloc(sizeof(t_value) * tt->cant_columns );
	for(i=0;i<tt->cant_columns;++i)
    {  	
  	  t_column col = ((t_column *)tt->columns)[i];
  	  ((t_value *)(((t_object*)oobj)->col_value))[i].pos = i;
  	  ((t_value *)(((t_object*)oobj)->col_value))[i].value = malloc(col.size);
    }
    ((t_object*)oobj)->isNewObj = true;//marcar como objeto nuevo, si se crea nueva instancia
	return oobj;
}
//----------------------------------------------------
void destroyObj(void *objFree)
{
	int i;
	t_object *obj = (t_object *)objFree;
	if(obj->destroyInternal!=NULL)
		obj->destroyInternal(obj);
    t_table *tt = obj->ds;
   
   for(i=0;i<tt->cant_columns;++i)
   {
   	free(((t_value *)obj->col_value)[i].value);//liberar punteros de los valores
   } 
    free(obj->col_value);
	free(obj);
}
//----------------------------------------------------
void destroyObjList(void **list,int size)
{
	int i=0;
	for(i=0;i< size;++i)
	{
		destroyObj(list[i]);		
	}
	free(list);
}
//----------------------------------------------------
void listObj(void *objList, char *criteria, int freeObj, void(*lstFunc)(void *o))//sobreescribir funcion toString....
{
	t_object *obj = (t_object *)objList;
	int i=0,size=0;
	void *list;
	t_object *rw;
    size = obj->findAll(obj,&list,NULL);
	for(i=0;i<size;++i)
	{
		rw = ((t_object **)list)[i];
		if(lstFunc!=NULL)
			lstFunc(rw);
		else
  		    rw->toString(rw);
	}
	if(freeObj)
	{
		destroyObjList(list,size);
		destroyObj(objList);
	}
}

void setId_Impl(void *self,int key)
{
   setValue(self,0,&key);
}
