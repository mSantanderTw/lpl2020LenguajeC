//----------------------------------------------------
// IMPLEMENTACION PARA DAR SOPORTE A COMPORTAMIENTO GENERICO - ABSTRACT.
// Implementacion en  "orm.c"
// manejo interno de cadenas SQL para Seleccionar por criterio, por id, Insertar Actualizar 
char *selectSQL(t_object *o);
char *getUpdateSQL(void *self);
char *getInsertSQL(void *self);
char *getFindByKeySQL(void *self);
// manejo generico para guardar un pseudo objeto
bool saveObjImpl(void *,int*);
// manejo generico para buscar por Id
int findImpl(void *self);
// manejo generico para obtener valor de propiedad por posicion
void *getValue(void *self,int pos);
// manejo generico para poner valor a propiedad por posicion
void setValue(void *self, int pos, void *valset);
// manejo generico para poder obtener datos por posicion
void getValueByPos(void *self,char *cad, int pos);

// MACROS PARA REEMPLAZOS CODIGO EN FORMATO MAS SIMPLIFICADO, PARA USAR EN GETTERS DE ENTERO Y CADENA DE CARACTERES
#define RET_INT(pos) return *( (int *) getValue(self,pos));
#define RET_CAD(pos) return  (char *) getValue(self,pos);

