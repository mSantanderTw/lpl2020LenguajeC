#include "../../config/config.h"
#include "../utils/utils.h"
#include "../object/object.h"
#include "orm.h"
#include <string.h>
#include <malloc.h>
//----------------------------------------------------
int findAllImpl(
void *self, //puntero generico
void **list, //lista donde se devolvera el resultado
char *criteria //criterio a aplicar si se envia NULl, no se usa.
)
{
	return execute_get_DB(self,criteria,list,((t_object *)self)->sizeObj,((t_object *)self)->constructor);
}
//----------------------------------------------------
int findImpl(void *self)
{
	return execute_get_DB(self,NULL,NULL,NULL,NULL);
}
//----------------------------------------------------
int execute_get_DB(
t_object *self, //puntero de tipo t_object // representacion base
char *sql, //cadena sql
void **list, // lista de retorno
size_t t, //tama�o del objeto a almcenar
void *(*fcConstructor) () //puntero a funcion del constructor para crear instancias en la lista de salida.
)
{
    PGconn *conn;
    void *obj;
    bool doFindByKey = (fcConstructor == NULL?true:false);// si no se pasa referencia a Pseudo objeto invocador, es FindAll
	char cadsql[MAX_SQL];
	CLEAR(cadsql,MAX_SQL);
    //Conexion con la base de datos 
	if ((conn = connectdb(SERVER,PORT,DB,USER,PASS)) == NULL){
        exit(-1);
    }
    int tuplas=0,rowi=0;        
    PGresult *res; //Instancia para contener los datos de ejecucion de consulta
	    
    if(doFindByKey) 
	  strcpy(cadsql , (char*)getFindByKeySQL((t_object*)self));
    else
    {
    	t_table *tt = ((t_object*)self)->ds;  
  		strcpy(cadsql,selectSQL((t_object*)self));
  		WHERE_SQL(cadsql,sql);
	}
       
    res = PQexec(conn, cadsql);
    
	if(doFindByKey) 
	  free(sql);
    if (res != NULL && PGRES_TUPLES_OK == PQresultStatus(res)) {
        tuplas = PQntuples(res);
        // si tengo puntero a funcion del constructor estoy resolviendo FindAll, sino un findByKey
        if(!doFindByKey) 
		  *list = malloc(t * tuplas); // solicitar memoria para array de pseudo Objetos.
	
        if(tuplas!=0) 
		{
			// crear lista de pseudo objetos - dimensionar a la cantidad de tuplas            
            int i=0;
            for (rowi = 0; rowi<tuplas; rowi++) 
            {
              obj = (doFindByKey? self : fcConstructor());  
			  // para el invocador setear data desde la BD
              t_table *tt = ((t_object *)obj)->ds;
			  
			  // verificar si se trata de findAll o findByKey
			  for(i=0;i<tt->cant_columns;++i)
    		  {
    		    void *dato;
    		    t_column col = ((t_column *)tt->columns)[i];
    		    // pasar de formato desde un PGresult a columns (generico) sin distincion de la clase.
    		    dato = malloc(col.size);
				if(col.tipo == t_int || col.tipo == t_bool) 
    		      *((int*)dato) = atoi(PQgetvalue(res,rowi,i));
				
				if(col.tipo == t_varchar || col.tipo == t_date)
				  dato = rtrim(PQgetvalue(res,rowi,i),' ');
				
				if(col.tipo == t_float)
    		      *((float*)dato) = atof(PQgetvalue(res,rowi,i));
				
				setValue(obj,i,dato);
		 	  }
		 	  ((t_object*)self)->isNewObj=false;
		 	  if(!doFindByKey)
		 	    ((t_object **)*list)[rowi] = obj;
			}		
        }		
        PQclear(res);
   }
   disconnectdb(conn);
   return tuplas;
}
//----------------------------------------------------
char *selectSQL(t_object *o)
{
  t_table *tt = o->ds;
  int size=0,i=0;
  char *sql, str_where[MAX_WHERE_SQL],fields[MAX_SQL];

  sql = (char*)malloc(MAX_SQL*sizeof(char));
  //blanquear cadenas
  memset(sql, '\0',MAX_SQL);
  memset(fields, '\0',MAX_SQL);
  
  //ver si hay criterio para agregar 
  strcpy(sql,SQL_STR);
  //recorrer cada nombre de columna(armar listado de campos)
  for(i=0;i<tt->cant_columns;++i)
  {
     strcat(fields , ((t_column *)tt->columns)[i].nombre);
     strcat(fields,(i<tt->cant_columns-1)?",":"");
  }
  //reemplazar listado de campos y nombre de tabla.
  strcpy(sql ,(char*) str_replace(sql,TOK1,fields));  
  strcpy(sql ,(char*)str_replace(sql,TOK2,tt->nombre));
  return sql;
}
//----------------------------------------------------
bool saveObjImpl(
void *self,  // puntero generico al que aplica
int *newId // En la insercion debe retornar el id nuevo.
)
{
   PGconn *conn;
   PGresult *res; 
   int code=0;
   char values[MAX_WHERE_SQL], where[MAX_WHERE_SQL],*sql;   
   t_object *obj = (t_object *) self;
    //Conexion con la base de datos 
   if ((conn = connectdb(SERVER,PORT,DB,USER,PASS))== NULL) {
      exit (-1); 
   }
   //Si esta marcado como nuevo objeto, es decir se creo instancia y no fue obtenido de la BD,
   if(obj->getIsNewObj(obj))
   {// insert
		sql = (char*)getInsertSQL(obj);
		res = PQexec(conn, sql);
		code = PQresultStatus(res);
		*newId = atoi(PQgetvalue(res,0,0));	
		PQclear(res);
		free(sql);
		obj->isNewObj=false;// marcar que esta guardado en la BD.
  }
  else
  {// update
      sql = (char*)getUpdateSQL(obj);
      res = PQexec(conn, sql) ;
      code = PQresultStatus(res);
      PQclear(res);	  
	  free(sql);
  }
  if ( code != PGRES_COMMAND_OK)  {
        disconnectdb(conn);     
       return false;
  }
    else
    {
       disconnectdb(conn);
       return true;    
    }
}
//----------------------------------------------------
// Obtener cadena SQL con Where por Clave primaria.
//----------------------------------------------------
char *getFindByKeySQL(void *self)
{
  t_table *tt = ((t_object*)self)->ds;
  int size=0,i=0;
  char *sql,where[MAX_WHERE_SQL];  
  CLEAR(where,MAX_WHERE_SQL);
  sql = (char *)malloc(sizeof(char)*MAX_SQL); //liberar despues de usar
  //blanquear cadena
  CLEAR(sql,MAX_SQL);
  strcat(sql,selectSQL((t_object*)self));
  //verificar columnas key de la clase para armar el where de la consulta sql.
  for(i=0;i<tt->cant_columns;++i)
  {
    if(((t_column *)tt->columns)[i].pkey)
    {
     snprintf(where, MAX_WHERE_SQL,"%s=", ((t_column *)tt->columns)[i].nombre);
     getValueByPos(self,where, i);// obtener value en forma generica desde configuracion de las columnas dadas en el .h de la clase
    }
  }
  WHERE_SQL(sql,where);
  return sql;
}
//----------------------------------------------------
char *getUpdateSQL(void *self)
{
  t_table *tt = ((t_object*)self)->ds;
  int size=0,i=0;
  char *sqlRet,sql[MAX_SQL],fields[MAX_WHERE_SQL],where[MAX_WHERE_SQL],field[MAX_WHERE_SQL],values[MAX_WHERE_SQL];
      
  //blanquear cadenas
  CLEAR(sql,MAX_SQL);
  CLEAR(where,MAX_WHERE_SQL);
  CLEAR(field,MAX_WHERE_SQL);
  CLEAR(fields,MAX_WHERE_SQL);
  CLEAR(values,MAX_WHERE_SQL);

  //ver si hay criterio para agregar 
  strcpy(sql,SQL_UPDATE);
  //cant de columnas clave y cant de columnas datos
  //recorrer cada nombre de columna(armar listado de campos y where)
  for(i=0;i<tt->cant_columns;++i)
  {
  	if(((t_column *)tt->columns)[i].pkey) // campo clave conforma el where donde aplicar el cambio
  	{
  	  snprintf( where , MAX_WHERE_SQL,"%s=", ((t_column *)tt->columns)[i].nombre);
  	  getValueByPos(self, where, i);
	  strcat(values,where);
	  strcat(values," and ");  
    }
	else // no clave son seteos 
	{
		snprintf(field, MAX_WHERE_SQL,"%s=", ((t_column *)tt->columns)[i].nombre);
		getValueByPos(self, field, i);
	    strcat(fields,field);
	    strcat(fields,",");
    } 	 
  } 
  //reemplazar listado de campos y nombre de tabla.
  strcpy(sql ,(char*)str_replace(sql,TOK1,tt->nombre));
  strcpy(sql ,(char*)str_replace(sql,TOK2,fields));
  strcpy(sql ,(char*)str_replace(sql,TOK3,values));
  strcpy(sql ,(char*)str_replace(sql,", where "," where "));
  strcpy(sql ,(char*)str_replace(sql," and ;",";"));
  sqlRet = (char*)malloc(strlen(sql)+1*sizeof(char*));
  strcpy(sqlRet,sql);
  return sqlRet;
}
//----------------------------------------------------
char *getInsertSQL(void *self)
{
  t_table *tt = ((t_object*)self)->ds;
  int size=0,i=0,isAutoNum=0;
  char *sqlRet,sql[MAX_SQL],fields[MAX_WHERE_SQL],where[MAX_WHERE_SQL],field[MAX_WHERE_SQL],values[MAX_WHERE_SQL],col_autonum[MAX];
  //blanquear cadenas
  CLEAR(sql,MAX_SQL);
  CLEAR(where,MAX_WHERE_SQL);
  CLEAR(field,MAX_WHERE_SQL);
  CLEAR(fields,MAX_WHERE_SQL);
  CLEAR(values,MAX_WHERE_SQL);
  CLEAR(col_autonum,MAX);
  
  //ver si hay criterio para agregar 
  strcpy(sql,SQL_INSERT);   
  //cant de columnas clave y cant de columnas datos
  //recorrer cada nombre de columna(armar listado de campos y where)
  for(i=0;i<tt->cant_columns;++i)
  {
     //si la columna es clave. generar consulta sql para obtener el siguiente id +1
     snprintf(field, MAX_WHERE_SQL,"%s", ((t_column *)tt->columns)[i].nombre);
     strcat(field, ",");
     strcat(fields,field);
     isAutoNum = isAutoNum || (((t_column *)tt->columns)[i].autonum);
     
     if(((t_column *)tt->columns)[i].pkey && ((t_column *)tt->columns)[i].autonum) //si es la columna clave primaria, debe cambiar el value del insert.
       {
         strcat(values,SQL_MAX_ID);
         strcpy(values ,(char*)str_replace(values,TOK1,((t_column *)tt->columns)[i].nombre));//reemplazar nombre de columna clave
         strcpy(values ,(char*)str_replace(values,TOK2,tt->nombre));//reemplazar nombre de tabla
         strcpy(col_autonum, ((t_column *)tt->columns)[i].nombre);
       }
     else
	   getValueByPos(self, values,i);     
     strcat(values, ",");
  }
  strcpy(sql ,(char*)str_replace(sql,TOK1,tt->nombre));
  strcpy(sql ,(char*)str_replace(sql,TOK2,fields));
  strcpy(sql ,(char*)str_replace(sql,TOK3,values));
  strcpy(sql ,(char*)str_replace(sql,",)",")"));
  if(!isAutoNum)
    strcpy(sql ,(char*)str_replace(sql," RETURNING $4;",";"));
  else
    strcpy(sql ,(char*)str_replace(sql,TOK4,col_autonum));
  sqlRet = (char*)malloc(strlen(sql)+1*sizeof(char*));
  strcpy(sqlRet,sql);
  return sqlRet;
}
//----------------------------------------------------
void *getValue(void *self,int pos)
{  
  t_object *obj =  (t_object *)self;
  t_value *val = &(((t_value*)obj->col_value)[pos]);
  return val->value;
}
//----------------------------------------------------
void setValue(void *self, int pos, void *valset)
{
  t_object *obj =  (t_object *)self;
  t_table *tt=obj->ds;
  t_value *vval = &(((t_value*)obj->col_value)[pos]);
  memcpy(vval->value,  valset,((t_column *)tt->columns)[pos].size);
}
//----------------------------------------------------
void getValueByPos(void *self,char *cad, int pos)
{
  char field[MAX_WHERE_SQL];
  t_object *obj =  (t_object *)self;
  t_table *tt=obj->ds;
  CLEAR(field,MAX_WHERE_SQL);
  t_value *dato = &(((t_value*)obj->col_value)[pos]);
  int datoInt = 0;
  char *datoChar;
  float datoFloat = 0.0;
  int tipo = ((t_column *)tt->columns)[pos].tipo;
  
  if( tipo == t_int || tipo == t_bool )
  {
  	datoInt = *((int*)(dato->value)); 
  	snprintf( field, MAX_WHERE_SQL, FRMTYPE(tipo), datoInt);
  }
  if( tipo == t_varchar || tipo == t_date )
  {
  	datoChar = ((char*)(dato->value)); 
  	snprintf( field, MAX_WHERE_SQL, FRMTYPE(tipo), datoChar);
  }
  if( tipo == t_float )
  {
  	datoFloat = *((float*)(dato->value)); 
  	snprintf( field, MAX_WHERE_SQL, FRMTYPE(tipo), datoFloat);
  }
  strcat(cad,field);
}

