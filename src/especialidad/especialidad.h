#ifndef ESP_INCLUDED
	#define ESP_INCLUDED
	#define CNT_COL_ESP 2
	#define POS_ID 0
	#define POS_DESCRIPCION 1
	//----------------------------------------------------
	typedef struct {
	    TYPEDEFCOMMON//// FIN CORRESPONDENCIA COMUN CON t_object		
	    //-- getters
	    getPropertyIntPtr getEspecialidadId;
		getPropertyCharPtr getDescripcion;
	    //-- setters
	    setPropertyIntPtr setEspecialidadId;
		setPropertyCharPtr setDescripcion;		
	}obj_Especialidad;
	// funcionalidad publica que se implementa en Especialidad.c
	extern obj_Especialidad *Especialidad_new ();
	// meta data para acceder a Especialidads - definicion de las columnas de la tabla
	static t_column cols_Especialidad[CNT_COL_ESP]=
	{ 
	//definicion del mapeo, por cada columna de la tabla con los atributos del pseudobjeto.
	// nombre de la columna, tipo, tama�o, si es clave, si es autonumerado.
	{"id",t_int,sizeof(int),true,true}, 
	{"descripcion",t_varchar,(sizeof(char)*MAX50)+1,false,false}
	};
	// plantilla para la Especialidad.
	static t_table table_Especialidad={"especialidad",CNT_COL_ESP,0, cols_Especialidad,NULL};
	typedef obj_Especialidad *(*getEspecialidadObjPtr)(void *self);
#endif
