#include "../../includes/includelib.h"
#include "especialidad.h"

THIS(obj_Especialidad)// crea definicion de funcion this para este modulo. .. Macro en config.h
//----------------------------------------------------
static void toString_EspecialidadImpl(void *self)
{
     obj_Especialidad *obj=this(self);
     printf("Especialidad_id: %d  Especialidad:%s \n",obj->getEspecialidadId(obj),obj->getDescripcion(obj));
}
//----------------------------------------------------
//implementacion de getters
//----------------------------------------------------
static int getEspecialidadId_Impl(void *self)
{
  RET_INT(POS_ID)
}
//----------------------------------------------------
static char *getDescripcionEspecialidad_Impl(void *self)
{
  RET_CAD(POS_DESCRIPCION)
}
//----------------------------------------------------
//implementacion setters
//----------------------------------------------------
static void setEspecialidadId_Impl(void *self,int val)
{ 
	setValue(self,POS_ID,&val);
}
//----------------------------------------------------
static void setDescripcionEspecialidad_Impl(void *self,char *descripcion_Especialidad)
{ 
	setValue(self,POS_DESCRIPCION,descripcion_Especialidad);
}
//----------------------------------------------------
//implementacion de relaciones
//----------------------------------------------------
/// ....
//----------------------------------------------------
//implementacion constructor
//----------------------------------------------------
static void *init_Especialidad(void *self)
{
  obj_Especialidad *obj 	= this(self);
  obj->ds  					= &table_Especialidad;
  obj->constructor 			= Especialidad_new;
  obj->sizeObj 				= sizeof(obj_Especialidad*);
  // Inicializar handlers de getters y setters
  /// getters
  obj->getEspecialidadId  	= getEspecialidadId_Impl;
  obj->getDescripcion 		= getDescripcionEspecialidad_Impl;  
  /// setters  
  obj->setEspecialidadId    = setEspecialidadId_Impl;
  obj->setDescripcion 		= setDescripcionEspecialidad_Impl;
  //incializacion de la interfaz de la entidad
  INIT_COMMON

  obj->toString    			= toString_EspecialidadImpl;
  return obj;
}
//----------------------------------------------------
//constructor de Especialidad
obj_Especialidad *Especialidad_new()
{
  return (obj_Especialidad *)init_obj(sizeof(obj_Especialidad), init_Especialidad);
}
//----------------------------------------------------
