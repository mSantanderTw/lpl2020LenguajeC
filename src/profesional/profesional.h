#ifndef PROF_INCLUDED
#define PROF_INCLUDED
#define CNT_COL_PROF 8
	#define POS_ID 0
	#define POS_MATRICULA_PROF 1
	#define POS_DNI_PROF 2
	#define POS_APELLIDO_PROF 3
	#define POS_NOMBRES_PROF 4
	#define POS_DOMICILIO_PROF 5
	#define POS_TELEFONO_PROF 6
	#define POS_COD_LOC_PROF 7
	//----------------------------------------------------
	typedef struct {
	    TYPEDEFCOMMON//// FIN CORRESPONDENCIA COMUN CON t_object
	    //-- getters
	    getPropertyIntPtr  getProfesionalId;	    
		getPropertyCharPtr getMatricula;
		getPropertyIntPtr  getDni;
		getPropertyCharPtr getApellido;
		getPropertyCharPtr getNombres;
		getPropertyCharPtr getDomicilio;
		getPropertyCharPtr getTelefono;
		getPropertyIntPtr  getCodLocalidad;
	    //-- setters
	    setPropertyIntPtr  setProfesionalId;
	    setPropertyCharPtr setMatricula;
	    setPropertyIntPtr  setDni;
	    setPropertyCharPtr setApellido;
	    setPropertyCharPtr setNombres;
	    setPropertyCharPtr setDomicilio;
	    setPropertyCharPtr setTelefono;
	    setPropertyIntPtr  setCodLocalidad;
	    getLocalidadObjPtr getLocalidadObj;
	    obj_Localidad *localidad;
	}obj_Profesional;
	// funcionalidad publica que se implementa en Profesional.c
	extern obj_Profesional *Profesional_new ();
	// meta data para acceder a Profesionals - definicion de las columnas de la tabla
	static t_column cols_Profesional[CNT_COL_PROF]=
	{ 
	//definicion del mapeo, por cada columna de la tabla con los atributos del pseudobjeto.
	// nombre de la columna, tipo, tama�o, si es clave, si es autonumerado.
	{"id",t_int,sizeof(int),true,true}, 
	{"matricula",t_date,(sizeof(char)*MAX20)+1,false,false},
	{"dni",t_int,sizeof(int),false,false},
	{"apellido",t_varchar,(sizeof(char)*MAX50)+1,false,false},
	{"nombres",t_varchar,(sizeof(char)*MAX90)+1,false,false},
	{"domicilio",t_varchar,(sizeof(char)*MAX)+1,false,false},
	{"telefono",t_varchar,(sizeof(char)*MAX20)+1,false,false},
	{"cod_localidad",t_int,sizeof(int),false,false},
	};
	// plantilla para la Profesional.
	static t_table table_Profesional={"profesional",CNT_COL_PROF,0, cols_Profesional,NULL};
	typedef obj_Profesional *(*getProfesionalObjPtr)(void *self);
#endif
