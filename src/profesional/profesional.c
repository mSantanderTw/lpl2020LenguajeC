#include "../../includes/includelib.h"
#include "../localidad/localidad.h"
#include "profesional.h"

THIS(obj_Profesional)// crea definicion de funcion this para este modulo. .. Macro en config.h
//----------------------------------------------------
//----------------------------------------------------
static void toString_ProfesionalImpl(void *self)
{
     obj_Profesional *obj=this(self);     
     obj_Localidad *loc = obj->getLocalidadObj(obj);
     printf("Profesional_id: %d - MAT: %s - DNI: %d  - ApyNom:%s, %s \tLocalidad:%s\n",
	 obj->getProfesionalId(obj),
	 obj->getMatricula(obj),
	 obj->getDni(obj),
	 obj->getApellido(obj),
	 obj->getNombres(obj),
	 loc->getNombre(loc)	
	 );
}
//----------------------------------------------------
//implementacion de getters
//----------------------------------------------------
static int getProfesionalId_Impl(void *self)
{
  RET_INT(POS_ID)
}
//----------------------------------------------------
static char *getMatriculaProfesional_Impl(void *self)
{
  RET_CAD(POS_MATRICULA_PROF)
}
//----------------------------------------------------
static int getDniProfesional_Impl(void *self)
{
  RET_INT(POS_DNI_PROF)
}
//----------------------------------------------------
static char *getApellidoProfesional_Impl(void *self)
{
  RET_CAD(POS_APELLIDO_PROF)
}
//----------------------------------------------------
static char *getNombresProfesional_Impl(void *self)
{
  RET_CAD(POS_NOMBRES_PROF)
}
//----------------------------------------------------
static char *getDomicilioProfesional_Impl(void *self)
{
  RET_CAD(POS_DOMICILIO_PROF)
}
//----------------------------------------------------
static char *getTelefonoProfesional_Impl(void *self)
{
  RET_CAD(POS_TELEFONO_PROF)
}
//----------------------------------------------------
static int getCodLocalidadProfesional_Impl(void *self)
{
	RET_INT(POS_COD_LOC_PROF)
}
//----------------------------------------------------
//implementacion setters
//----------------------------------------------------
static void setProfesionalId_Impl(void *self,int val)
{ 
	setValue(self,POS_ID,&val);
}
//----------------------------------------------------
static void setMatriculaProfesional_Impl(void *self,char *mat_Profesional)
{ 
	setValue(self,POS_DNI_PROF,mat_Profesional);
}
//----------------------------------------------------
static void setDniProfesional_Impl(void *self,int val)
{ 
	setValue(self,POS_DNI_PROF,&val);
}
//----------------------------------------------------
static void setApellidoProfesional_Impl(void *self,char *apellido_Profesional)
{ 
	setValue(self,POS_APELLIDO_PROF,apellido_Profesional);
}
//----------------------------------------------------
static void setNombresProfesional_Impl(void *self,char *nombres_Profesional)
{ 
	setValue(self,POS_NOMBRES_PROF,nombres_Profesional);
}
//----------------------------------------------------
static void setDomicilioProfesional_Impl(void *self,char *domicilio_Profesional)
{ 
	setValue(self,POS_DOMICILIO_PROF,domicilio_Profesional);
}
//----------------------------------------------------
static void setTelefonoProfesional_Impl(void *self,char *telefono_Profesional)
{ 
	setValue(self,POS_TELEFONO_PROF,telefono_Profesional);
}
//----------------------------------------------------
static void setCodLocalidadProfesional_Impl(void *self,int val)
{ 
	setValue(self,POS_COD_LOC_PROF,&val);
}
//----------------------------------------------------
static void destroyInternalProf_Impl(void *self)
{
	obj_Profesional *obj = this(self);
	
	if(obj->localidad!=NULL)
	  destroyObj(obj->localidad);  
}
//----------------------------------------------------
//implementacion de relaciones
//----------------------------------------------------
/// ....
obj_Localidad *getLocalidadProfObj_Impl(void *self)
{
	obj_Profesional *obj = this(self);
	if(obj->getCodLocalidad(obj)!=0)
	  {
	  	obj->localidad = Localidad_new();
	  	if(!obj->localidad->findbykey(obj->localidad,obj->getCodLocalidad(obj)))
	  	  obj->localidad = NULL;
	  }
	return obj->localidad;
}
//----------------------------------------------------
//implementacion constructor
//----------------------------------------------------
static void *init_Profesional(void *self)
{
  obj_Profesional *obj    = this(self);
  obj->ds  			      = &table_Profesional;
  obj->constructor 	      = Profesional_new;
  obj->sizeObj 		      = sizeof(obj_Profesional*);
  obj->localidad 	      = NULL;
  // Inicializar handlers de getters y setters
  /// getters
  obj->getProfesionalId   = getProfesionalId_Impl;
  obj->getMatricula       = getMatriculaProfesional_Impl;
  obj->getDni             = getDniProfesional_Impl;
  obj->getApellido        = getApellidoProfesional_Impl;
  obj->getNombres         = getNombresProfesional_Impl;
  obj->getDomicilio       = getNombresProfesional_Impl;
  obj->getTelefono        = getTelefonoProfesional_Impl;
  obj->getCodLocalidad    = getCodLocalidadProfesional_Impl;  
  /// setters  
  obj->setProfesionalId   = setProfesionalId_Impl;
  obj->setMatricula       = setMatriculaProfesional_Impl;
  obj->setDni             = setDniProfesional_Impl;
  obj->setApellido        = setApellidoProfesional_Impl;
  obj->setNombres         = setNombresProfesional_Impl;
  obj->setDomicilio       = setDomicilioProfesional_Impl;
  obj->setTelefono        = setTelefonoProfesional_Impl;
  obj->setCodLocalidad    = setCodLocalidadProfesional_Impl;
  //incializacion de la interfaz de la entidad
  INIT_COMMON

  obj->toString    		  = toString_ProfesionalImpl;
  // implementar detroy internal para liberar recursos  
  obj->destroyInternal 	  = destroyInternalProf_Impl;
  //---- acceso a relaciones  
  obj->getLocalidadObj 	  = getLocalidadProfObj_Impl;
  return obj;
}
//----------------------------------------------------
//constructor de Profesional
obj_Profesional *Profesional_new()
{
  return (obj_Profesional *)init_obj(sizeof(obj_Profesional), init_Profesional);
}
//----------------------------------------------------
