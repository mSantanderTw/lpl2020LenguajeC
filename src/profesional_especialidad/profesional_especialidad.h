#ifndef PROFESP_INCLUDED
	#define PROFESP_INCLUDED
	#define CNT_COL_PROFESP 4 
	#define POS_ID 0
	#define POS_COD_PROF_PE 1
	#define POS_COD_ESP_PE 2
	#define POS_FDESDE_PE 3
	//----------------------------------------------------
	typedef struct {
	    TYPEDEFCOMMON//// FIN CORRESPONDENCIA COMUN CON t_object
	    //-- getters
	    getPropertyIntPtr     getProfEspId;
	    getPropertyIntPtr     getCodProfesional;
	    getPropertyIntPtr     getCodEspecialidad;
		getPropertyCharPtr    getFechaDesde;
	    //-- setters
	    setPropertyIntPtr     setProfEspId;
	    setPropertyIntPtr     setCodProfesional;
	    setPropertyIntPtr     setCodEspecialidad;
	    setPropertyCharPtr    setFechaDesde;
	    getProfesionalObjPtr  getProfesionalObj;
	    getEspecialidadObjPtr getEspecialidadObj;
		
	    obj_Profesional *profesional;	    
	    obj_Especialidad *especialidad;
	}obj_ProfesionalEspecialidad;
	// funcionalidad publica que se implementa en ProfesionalEspecialidad.c
	extern obj_ProfesionalEspecialidad *ProfesionalEspecialidad_new ();
	// meta data para acceder a ProfesionalEspecialidads - definicion de las columnas de la tabla
	static t_column cols_ProfesionalEspecialidad[CNT_COL_PROFESP]=
	{ 
	//definicion del mapeo, por cada columna de la tabla con los atributos del pseudobjeto.
	// nombre de la columna, tipo, tama�o, si es clave, si es autonumerado.
	{"id",t_int,sizeof(int),true,true}, 
	{"cod_profesional",t_int,sizeof(int),false,false},
	{"cod_especialidad",t_int,sizeof(int),false,false},
	{"fecha_desde",t_varchar,(sizeof(char)*MAX50)+1,false,false}
	};
	// plantilla para la ProfesionalEspecialidad.
	static t_table table_ProfesionalEspecialidad={"profesional_especialidad",CNT_COL_PROFESP,0, cols_ProfesionalEspecialidad,NULL};
#endif
