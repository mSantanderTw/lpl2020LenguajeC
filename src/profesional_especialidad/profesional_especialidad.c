#include "../../includes/includelib.h"
#include "../especialidad/especialidad.h"
#include "../localidad/localidad.h"
#include "../profesional/profesional.h"
#include "profesional_especialidad.h"

THIS(obj_ProfesionalEspecialidad)// crea definicion de funcion this para este modulo. .. Macro en config.h
//----------------------------------------------------
//----------------------------------------------------
static void toString_ProfesionalEspecialidadImpl(void *self)
{
     obj_ProfesionalEspecialidad *obj=this(self);
     obj_Profesional *prof = obj->getProfesionalObj(obj);
     obj_Especialidad *esp = obj->getEspecialidadObj(obj);
     printf("ProfesionalEspecialidad_id: %d - \tProf:%s, %s \tEsp: %s\t - FechaDesde:%s \n",
	 obj->getProfEspId(obj),
	 prof->getApellido(prof),
	 prof->getNombres(prof),
	 esp->getDescripcion(esp),
	 obj->getFechaDesde(obj)
	 );
}
//----------------------------------------------------
//implementacion de getters
//----------------------------------------------------
static int getProfesionalEspecialidadId_Impl(void *self)
{
  RET_INT(POS_ID)
}
//----------------------------------------------------
static int getCodProfesional_PE_Impl(void *self)
{
  RET_INT(POS_COD_PROF_PE)
}
//----------------------------------------------------
static int getCodEspecialidad_PE_Impl(void *self)
{
  RET_INT(POS_COD_ESP_PE)
}
//----------------------------------------------------
static char *getFechaDesde_PE_Impl(void *self)
{
  RET_CAD(POS_FDESDE_PE)
}
//----------------------------------------------------
//implementacion setters
//----------------------------------------------------
static void setProfesionalEspecialidadId_Impl(void *self,int val)
{ 
	setValue(self,POS_ID,&val);
}
//----------------------------------------------------
static void setCodProfesional_PE_Impl(void *self,int val)
{ 
	setValue(self,POS_COD_PROF_PE,&val);
}
//----------------------------------------------------
static void setCodEspecialidad_PE_Impl(void *self,int val)
{ 
	setValue(self,POS_COD_ESP_PE,&val);
}
//----------------------------------------------------
static void setFechaDesde_PE_Impl(void *self,char *fecha_desde)
{ 
	setValue(self,POS_FDESDE_PE,fecha_desde);
}
//----------------------------------------------------
static void destroyInternalProfEsp_Impl(void *self)
{
	///IMPLEMENTAR....
	return ;
}
//----------------------------------------------------
//implementacion de relaciones
//----------------------------------------------------
/// ....
obj_Profesional *getProfesionalProfEspObj_Impl(void *self)
{
	///IMPLEMENTAR....
	return NULL;
}
//----------------------------------------------------
obj_Especialidad *getEspecialidadProfEspObj_Impl(void *self)
{
	///IMPLEMENTAR....
	return NULL;
}
//----------------------------------------------------
//implementacion constructor
//----------------------------------------------------
static void *init_ProfesionalEspecialidad(void *self)
{
  obj_ProfesionalEspecialidad *obj = this(self);
  obj->ds                          = &table_ProfesionalEspecialidad;
  obj->constructor                 = ProfesionalEspecialidad_new;
  obj->sizeObj                     = sizeof(obj_ProfesionalEspecialidad*);
  obj->profesional				   = NULL;
  obj->especialidad				   = NULL;
  // Inicializar handlers de getters y setters
  /// getters
  obj->getProfEspId  	           = getProfesionalEspecialidadId_Impl;
  obj->getCodProfesional           = getCodProfesional_PE_Impl;
  obj->getCodEspecialidad          = getCodEspecialidad_PE_Impl;
  obj->getFechaDesde               = getFechaDesde_PE_Impl;  
  /// setters  
  obj->setProfEspId                = setProfesionalEspecialidadId_Impl;
  obj->setCodProfesional           = setCodProfesional_PE_Impl;
  obj->setCodEspecialidad          = setCodEspecialidad_PE_Impl;
  obj->setFechaDesde               = setFechaDesde_PE_Impl;
  //incializacion de la interfaz de la entidad
  INIT_COMMON
  
  obj->toString                    = toString_ProfesionalEspecialidadImpl;
  // implementar detroy internal para liberar recursos  
  obj->destroyInternal             = destroyInternalProfEsp_Impl;
  //---- acceso a relaciones    
  obj->getProfesionalObj		   = getProfesionalProfEspObj_Impl;
  obj->getEspecialidadObj		   = getEspecialidadProfEspObj_Impl; 
  
  return obj;
}
//----------------------------------------------------
//constructor de ProfesionalEspecialidad
obj_ProfesionalEspecialidad *ProfesionalEspecialidad_new()
{
  return (obj_ProfesionalEspecialidad *)init_obj(sizeof(obj_ProfesionalEspecialidad), init_ProfesionalEspecialidad);
}
//----------------------------------------------------
