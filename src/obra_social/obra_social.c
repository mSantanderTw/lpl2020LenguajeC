#include "../../includes/includelib.h"
#include "obra_social.h"

THIS(obj_ObraSocial)// crea definicion de funcion this para este modulo. .. Macro en config.h
//----------------------------------------------------
static void toString_ObraSocialImpl(void *self)
{
     obj_ObraSocial *obj=this(self);
     printf("id: %d  Descripcion:%s \t Domicilio: %s \n",
	 obj->getObraSocialId(obj),
	 obj->getDescripcion(obj),
	 obj->getDomicilio(obj)
	 );
}
//----------------------------------------------------
//implementacion de getters
//----------------------------------------------------
static int getObraSocialId_Impl(void *self)
{
  RET_INT(POS_ID)
}
//----------------------------------------------------
static char *getDescripcionObraSocial_Impl(void *self)
{
  RET_CAD(POS_DESCRIPCION)
}
//----------------------------------------------------
static char *getDomicilioObraSocial_Impl(void *self)
{
  RET_CAD(POS_DOMICILIO)
}
//----------------------------------------------------
//implementacion setters
//----------------------------------------------------
static void setObraSocialId_Impl(void *self,int val)
{ 
	setValue(self,POS_ID,&val);
}
//----------------------------------------------------
static void setDescripcionObraSocial_Impl(void *self,char *descripcion_ObraSocial)
{ 
	setValue(self,POS_DESCRIPCION,descripcion_ObraSocial);
}
//----------------------------------------------------
static void setDomicilioObraSocial_Impl(void *self,char *domicilio_ObraSocial)
{ 
	setValue(self,POS_DOMICILIO,domicilio_ObraSocial);
}
//----------------------------------------------------
//implementacion de relaciones
//----------------------------------------------------
/// ....
//----------------------------------------------------
//implementacion constructor
//----------------------------------------------------
static void *init_ObraSocial(void *self)
{
  obj_ObraSocial *obj   = this(self);
  obj->ds  				= &table_ObraSocial;
  obj->constructor 		= ObraSocial_new;
  obj->sizeObj 			= sizeof(obj_ObraSocial*);
  // Inicializar handlers de getters y setters
  /// getters
  obj->getObraSocialId  = getObraSocialId_Impl;
  obj->getDescripcion	= getDescripcionObraSocial_Impl;  
  obj->getDomicilio		= getDomicilioObraSocial_Impl;  
  /// setters  
  obj->setObraSocialId  = setObraSocialId_Impl;
  obj->setDescripcion	= setDescripcionObraSocial_Impl;
  obj->setDomicilio		= setDomicilioObraSocial_Impl;
  //incializacion de la interfaz de la entidad
  INIT_COMMON
  
  obj->toString    		= toString_ObraSocialImpl;
  return obj;
}
//----------------------------------------------------
//constructor de ObraSocial
obj_ObraSocial *ObraSocial_new()
{
  return (obj_ObraSocial *)init_obj(sizeof(obj_ObraSocial), init_ObraSocial);
}
//----------------------------------------------------
