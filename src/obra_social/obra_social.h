#ifndef OS_INCLUDED
	#define OS_INCLUDED
	#define CNT_COL_OS 3
	#define POS_ID 0
	#define POS_DESCRIPCION 1
	#define POS_DOMICILIO 2
	//----------------------------------------------------
	typedef struct {
	    TYPEDEFCOMMON//// FIN CORRESPONDENCIA COMUN CON t_object
	    //-- getters
	    getPropertyIntPtr  getObraSocialId;
		getPropertyCharPtr getDescripcion;
		getPropertyCharPtr getDomicilio;
	    //-- setters
	    setPropertyIntPtr  setObraSocialId;
		setPropertyCharPtr setDescripcion;  
		setPropertyCharPtr setDomicilio;  
	}obj_ObraSocial;
	// funcionalidad publica que se implementa en ObraSocial.c
	extern obj_ObraSocial *ObraSocial_new ();
	// meta data para acceder a ObraSocials - definicion de las columnas de la tabla
	static t_column cols_ObraSocial[CNT_COL_OS]=
	{ 
	//definicion del mapeo, por cada columna de la tabla con los atributos del pseudobjeto.
	// nombre de la columna, tipo, tama�o, si es clave, si es autonumerado.
	{"id",t_int,sizeof(int),true,true}, 
	{"descripcion",t_varchar,(sizeof(char)*MAX50)+1,false,false},
	{"domicilio_principal",t_varchar,(sizeof(char)*MAX90)+1,false,false}
	};
	// plantilla para la ObraSocial.
	static t_table table_ObraSocial={"obra_social",CNT_COL_OS,0, cols_ObraSocial,NULL};
	typedef obj_ObraSocial *(*getObraSocialObjPtr)(void *self);
#endif
