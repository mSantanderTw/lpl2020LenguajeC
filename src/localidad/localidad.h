#ifndef LOC_INCLUDED
	#define LOC_INCLUDED
	#define CNT_COL_LOC 2
	#define POS_ID 0
	#define POS_DESCRIPCION 1
	//----------------------------------------------------
	typedef struct {
	    TYPEDEFCOMMON//// FIN CORRESPONDENCIA COMUN CON t_object
	    //-- getters
	    getPropertyIntPtr getLocalidadId;
		getPropertyCharPtr getNombre;	    
	    //-- setters
	    setPropertyIntPtr setLocalidadId;
		setPropertyCharPtr setNombre;  
	}obj_Localidad;
	// funcionalidad publica que se implementa en Localidad.c
	extern obj_Localidad *Localidad_new ();
	// meta data para acceder a Localidads - definicion de las columnas de la tabla
	static t_column cols_Localidad[CNT_COL_LOC]=
	{ 
	//definicion del mapeo, por cada columna de la tabla con los atributos del pseudobjeto.
	// nombre de la columna, tipo, tama�o, si es clave, si es autonumerado.
	{"id",t_int,sizeof(int),true,true}, 
	{"nombre",t_varchar,(sizeof(char)*MAX50)+1,false,false}
	};
	// plantilla para la Localidad.
	static t_table table_Localidad={"localidad",CNT_COL_LOC,0, cols_Localidad,NULL};
	typedef obj_Localidad *(*getLocalidadObjPtr)(void *self);
#endif
