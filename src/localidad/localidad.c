#include "../../includes/includelib.h"
#include "localidad.h"

THIS(obj_Localidad)// crea definicion de funcion this para este modulo. .. Macro en config.h
//----------------------------------------------------
static void toString_LocalidadImpl(void *self)
{
     obj_Localidad *obj=this(self);
     printf("Localidad_id: %d  Localidad:%s \n",
	 obj->getLocalidadId(obj),
	 obj->getNombre(obj)
	 );
}
//----------------------------------------------------
//implementacion de getters
//----------------------------------------------------
static int getLocalidadId_Impl(void *self)
{
  RET_INT(POS_ID)
}
//----------------------------------------------------
static char *getNombreLocalidad_Impl(void *self)
{
  RET_CAD(POS_DESCRIPCION)
}
//----------------------------------------------------
//implementacion setters
//----------------------------------------------------
static void setLocalidadId_Impl(void *self,int val)
{ 
	setValue(self,POS_ID,&val);
}
//----------------------------------------------------
static void setNombreLocalidad_Impl(void *self,char *descripcion_Localidad)
{ 
	setValue(self,POS_DESCRIPCION,descripcion_Localidad);
}
//----------------------------------------------------
//implementacion de relaciones
//----------------------------------------------------
/// ....
//----------------------------------------------------
//implementacion constructor
//----------------------------------------------------
static void *init_Localidad(void *self)
{
  obj_Localidad *obj 	  = this(self);
  obj->ds  				  = &table_Localidad;
  obj->constructor 		  = Localidad_new;
  obj->sizeObj 			  = sizeof(obj_Localidad*);
  // Inicializar handlers de getters y setters
  /// getters
  obj->getLocalidadId  	  = getLocalidadId_Impl;
  obj->getNombre		  = getNombreLocalidad_Impl;  
  /// setters  
  obj->setLocalidadId     = setLocalidadId_Impl;
  obj->setNombre   		  = setNombreLocalidad_Impl;
  //incializacion de la interfaz de la entidad
  INIT_COMMON
  
  obj->toString    		  = toString_LocalidadImpl;
  return obj;
}
//----------------------------------------------------
//constructor de Localidad
obj_Localidad *Localidad_new()
{
  return (obj_Localidad *)init_obj(sizeof(obj_Localidad), init_Localidad);
}
//----------------------------------------------------
