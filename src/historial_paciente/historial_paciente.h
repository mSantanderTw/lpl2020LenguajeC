#ifndef HIST_PAC_INCLUDED
	#define HIST_PAC_INCLUDED
	#define CNT_COL_HP 6
	#define POS_ID 0
	#define POS_COD_PACIENTE_HP 1
	#define POS_COD_DIAGNOSTICO_HP 2
	#define POS_FECHA_HP 3
	#define POS_INDICACIONES_HP 4
	#define POS_OBSERVACIONES_HP 5
	//----------------------------------------------------
	typedef struct {
	    TYPEDEFCOMMON //// FIN CORRESPONDENCIA COMUN CON t_object
	    //-- getters
	    getPropertyIntPtr 		 getHistorialPacienteId;
	    getPropertyIntPtr 		 getCodPaciente;
	    getPropertyIntPtr 		 getCodTipoDiagnostico;
	    getPropertyCharPtr 		 getFecha;
	    getPropertyCharPtr 		 getIndicaciones;
	    getPropertyCharPtr 		 getObservaciones;	    
	    //-- setters
	    setPropertyIntPtr 		 setHistorialPacienteId;
	    setPropertyIntPtr 		 setCodPaciente;
	    setPropertyIntPtr 		 setCodTipoDiagnostico;
		setPropertyCharPtr 		 setFecha;	
		setPropertyCharPtr 		 setIndicaciones;
		setPropertyCharPtr 		 setObservaciones;
	    getPacienteObjPtr 		 getPacienteObj;		
		getTipoDiagnosticoObjPtr getTipoDiagnosticoObj;
	    obj_Paciente *paciente;	    
		obj_TipoDiagnostico *tipo_diagnostico;
	}obj_HistorialPaciente;
	// funcionalidad publica que se implementa en HistorialPaciente.c
	extern obj_HistorialPaciente *HistorialPaciente_new();
	// meta data para acceder a HistorialPacientes - definicion de las columnas de la tabla
	static t_column cols_HistorialPaciente[CNT_COL_HP]=
	{ 
	//definicion del mapeo, por cada columna de la tabla con los atributos del pseudobjeto.
	// nombre de la columna, tipo, tama�o, si es clave, si es autonumerado.
	{"id",t_int,sizeof(int),true,true}, 
	{"cod_paciente",t_int,sizeof(int),false,false},
	{"cod_diagnostico",t_int,sizeof(int),false,false},
	{"fecha",t_varchar,(sizeof(char)*MAX10)+1,false,false},
	{"indicaciones",t_varchar,(sizeof(char)*MAX120)+1,false,false},
	{"observaciones",t_varchar,(sizeof(char)*MAX150)+1,false,false}
	};
	// plantilla para la HistorialPaciente.
	static t_table table_HistorialPaciente={"historial_paciente",CNT_COL_HP,0, cols_HistorialPaciente,NULL};
#endif
