#include "../../includes/includelib.h"
#include "../localidad/localidad.h"
#include "../obra_social/obra_social.h"
#include "../tipo_diagnostico/tipo_diagnostico.h"
#include "../paciente/paciente.h"
#include "historial_paciente.h"

THIS(obj_HistorialPaciente)// crea definicion de funcion this para este modulo. .. Macro en config.h
//----------------------------------------------------
static void toString_HistorialPacienteImpl(void *self)
{
     obj_HistorialPaciente *obj=this(self);
     obj_Paciente *pac = obj->getPacienteObj(obj);
     obj_TipoDiagnostico *tdiag = obj->getTipoDiagnosticoObj(obj);
     printf("HistorialPaciente_id: %d - \tPac:%s,%s\t - Diagnostico:%s \tFechaDesde:%s \n",
	 obj->getHistorialPacienteId(obj),
	 pac->getApellido(pac),
	 pac->getNombres(pac),
	 tdiag->getDescripcion(tdiag),
	 obj->getFecha(obj)
	 );
}
//----------------------------------------------------
//implementacion de getters
//----------------------------------------------------
static int getHistorialPacienteId_Impl(void *self)
{
  RET_INT(POS_ID)
}
//----------------------------------------------------
static int getCodPaciente_HP_Impl(void *self)
{
  RET_INT(POS_COD_PACIENTE_HP)
}
//----------------------------------------------------
static int getCodTipoDiagnostico_HP_Impl(void *self)
{
  RET_INT(POS_COD_DIAGNOSTICO_HP)
}
//----------------------------------------------------
static char *getFecha_HP_Impl(void *self)
{
  RET_CAD(POS_FECHA_HP)
}
//----------------------------------------------------
//implementacion setters
//----------------------------------------------------
static void setHistorialPacienteId_Impl(void *self,int val)
{ 
	setValue(self,POS_ID,&val);
}
//----------------------------------------------------
static void setCodPaciente_HP_Impl(void *self,int val)
{ 
	setValue(self,POS_COD_PACIENTE_HP,&val);
}
//----------------------------------------------------
static void setCodTipoDiagnostico_HP_Impl(void *self,int val)
{ 
	setValue(self,POS_COD_DIAGNOSTICO_HP,&val);
}
//----------------------------------------------------
static void setFecha_HP_Impl(void *self,char *fecha_desde)
{ 
	setValue(self,POS_FECHA_HP,fecha_desde);
}
//----------------------------------------------------
static void destroyInternalHistPac_Impl(void *self)
{
	obj_HistorialPaciente *obj = this(self);
	
	if(obj->paciente!=NULL)
	  destroyObj(obj->paciente);
	if(obj->tipo_diagnostico!=NULL)
	  destroyObj(obj->tipo_diagnostico);
}
//----------------------------------------------------
//implementacion de relaciones
//----------------------------------------------------
/// ....
obj_Paciente *getPacienteHistPacObj_Impl(void *self)
{
	///IMPLEMENTAR....
	return NULL;
}
//----------------------------------------------------
obj_TipoDiagnostico *getTipoDiagnosticoHistPacObj_Impl(void *self)
{
	///IMPLEMENTAR....
	return NULL;
}
//----------------------------------------------------
//implementacion constructor
//----------------------------------------------------
static void *init_HistorialPaciente(void *self)
{
  obj_HistorialPaciente *obj  = this(self);
  obj->ds                     = &table_HistorialPaciente;
  obj->constructor            = HistorialPaciente_new;
  obj->sizeObj                = sizeof(obj_HistorialPaciente*);
  obj->paciente				  = NULL;
  obj->tipo_diagnostico		  = NULL;
  // Inicializar handlers de getters y setters
  /// getters
  obj->getHistorialPacienteId = getHistorialPacienteId_Impl;
  obj->getCodPaciente         = getCodPaciente_HP_Impl;
  obj->getCodTipoDiagnostico  = getCodTipoDiagnostico_HP_Impl;
  obj->getFecha               = getFecha_HP_Impl;  
  /// setters  
  obj->setHistorialPacienteId = setHistorialPacienteId_Impl;
  obj->setCodPaciente         = setCodPaciente_HP_Impl;
  obj->setCodTipoDiagnostico  = setCodTipoDiagnostico_HP_Impl;
  obj->setFecha               = setFecha_HP_Impl;
  //incializacion de la interfaz de la entidad
  INIT_COMMON
  
  obj->toString               = toString_HistorialPacienteImpl;
  // implementar detroy internal para liberar recursos
  obj->destroyInternal        = destroyInternalHistPac_Impl;
  //---- acceso a relaciones    
  obj->getPacienteObj		  = getPacienteHistPacObj_Impl;
  obj->getTipoDiagnosticoObj  = getTipoDiagnosticoHistPacObj_Impl;
  return obj;
}
//----------------------------------------------------
//constructor de HistorialPaciente
obj_HistorialPaciente *HistorialPaciente_new()
{
  return (obj_HistorialPaciente *)init_obj(sizeof(obj_HistorialPaciente), init_HistorialPaciente);
}
//----------------------------------------------------
