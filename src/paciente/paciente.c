#include "../../includes/includelib.h"
#include "../localidad/localidad.h"
#include "../obra_social/obra_social.h"
#include "paciente.h"

THIS(obj_Paciente)// crea definicion de funcion this para este modulo. .. Macro en config.h
//----------------------------------------------------
static void toString_PacienteImpl(void *self)
{
     obj_Paciente *obj=this(self);     
     obj_Localidad *loc = obj->getLocalidadObj(obj);
     obj_ObraSocial *os = obj->getObraSocialObj(obj);
     printf("Paciente_id: %d - Leg: %d - %s - DNI: %d  - ApyNom:%s, %s \tLocalidad:%s\tObra Soc: %s\n",
	 obj->getPacienteId(obj),
	 obj->getNroLegajo(obj),
	 obj->getFechaNac(obj),
	 obj->getDni(obj),
	 obj->getApellido(obj),
	 obj->getNombres(obj),
	 loc->getNombre(loc),
	 os->getDescripcion(os)
	 );
}
//----------------------------------------------------
//implementacion de getters
//----------------------------------------------------
static int getPacienteId_Impl(void *self)
{
  	RET_INT(POS_ID)
}
//----------------------------------------------------
static int getNroLegajoPaciente_Impl(void *self)
{
  	RET_INT(POS_NROLEGAJO_PAC)
}
//----------------------------------------------------
static char *getFechaNacPaciente_Impl(void *self)
{
  	RET_CAD(POS_FNAC_PAC)
}
//----------------------------------------------------
static int getDniPaciente_Impl(void *self)
{
  	RET_INT(POS_DNI_PAC)
}
//----------------------------------------------------
static char *getApellidoPaciente_Impl(void *self)
{
  	RET_CAD(POS_APELLIDO_PAC)
}
//----------------------------------------------------
static char *getNombresPaciente_Impl(void *self)
{
  	RET_CAD(POS_NOMBRES_PAC)
}
//----------------------------------------------------
static char *getDomicilioPaciente_Impl(void *self)
{
  	RET_CAD(POS_DOMICILIO_PAC)
}
//----------------------------------------------------
static char *getTelefonoPaciente_Impl(void *self)
{
  	RET_CAD(POS_TELEFONO_PAC)
}
//----------------------------------------------------
static int getCodObraSocialPaciente_Impl(void *self)
{
	RET_INT(POS_COD_OS_PAC)
}
//----------------------------------------------------
static int getCodLocalidadPaciente_Impl(void *self)
{
	RET_INT(POS_COD_LOC_PAC)
}
//----------------------------------------------------
//implementacion setters
//----------------------------------------------------
static void setPacienteId_Impl(void *self,int val)
{ 
	setValue(self,POS_ID,&val);
}
//----------------------------------------------------
static void setNroLegajoPaciente_Impl(void *self,int val)
{ 
	setValue(self,POS_NROLEGAJO_PAC,&val);
}
//----------------------------------------------------
static void setFechaNacPaciente_Impl(void *self,char *f_nac_Paciente)
{ 
	setValue(self,POS_FNAC_PAC,f_nac_Paciente);
}
//----------------------------------------------------
static void setDniPaciente_Impl(void *self,int val)
{ 
	setValue(self,POS_DNI_PAC,&val);
}
//----------------------------------------------------
static void setApellidoPaciente_Impl(void *self,char *apellido_Paciente)
{ 
	setValue(self,POS_APELLIDO_PAC,apellido_Paciente);
}
//----------------------------------------------------
static void setNombresPaciente_Impl(void *self,char *nombres_Paciente)
{ 
	setValue(self,POS_NOMBRES_PAC,nombres_Paciente);
}
//----------------------------------------------------
static void setDomicilioPaciente_Impl(void *self,char *domicilio_Paciente)
{ 
	setValue(self,POS_DOMICILIO_PAC,domicilio_Paciente);
}
//----------------------------------------------------
static void setTelefonoPaciente_Impl(void *self,char *telefono_Paciente)
{ 
	setValue(self,POS_TELEFONO_PAC,telefono_Paciente);
}
//----------------------------------------------------
static void setCodObraSocialPaciente_Impl(void *self,int val)
{ 
	setValue(self,POS_COD_OS_PAC,&val);
}
//----------------------------------------------------
static void setCodLocalidadPaciente_Impl(void *self,int val)
{ 
	setValue(self,POS_COD_LOC_PAC,&val);
}
//----------------------------------------------------
static void destroyInternalPac_Impl(void *self)
{
	obj_Paciente *obj = this(self);
	
	if(obj->localidad!=NULL)
	  destroyObj(obj->localidad);
	if(obj->obrasoc!=NULL)
	  destroyObj(obj->obrasoc);
}
//----------------------------------------------------
//implementacion de relaciones
//----------------------------------------------------
/// ....
obj_Localidad *getLocalidadPacienteObj_Impl(void *self)
{
	obj_Paciente *obj = this(self);
	if(obj->getCodLocalidad(obj)!=0)
	  {
	  	obj->localidad = Localidad_new();
	  	if(!obj->localidad->findbykey(obj->localidad,obj->getCodLocalidad(obj)))
	  	  obj->localidad = NULL;
	  }
	return obj->localidad;
}
//----------------------------------------------------
obj_ObraSocial *getObraSocialPacienteObj_Impl(void *self)
{
	obj_Paciente *obj = this(self);
	if(obj->getCodObraSocial(obj)!=0)
	  {
	  	obj->obrasoc = ObraSocial_new();
	  	if(!obj->obrasoc->findbykey(obj->obrasoc,obj->getCodObraSocial(obj)))
	  	  obj->obrasoc = NULL;
	  }
	return obj->obrasoc;
}
//----------------------------------------------------
//implementacion constructor
//----------------------------------------------------
static void *init_Paciente(void *self)
{
  obj_Paciente *obj     = this(self);
  obj->ds  			    = &table_Paciente;
  obj->constructor 	    = Paciente_new;
  obj->sizeObj 		    = sizeof(obj_Paciente*);
  obj->localidad 	    = NULL;
  obj->obrasoc 	 	    = NULL;
  // Inicializar handlers de getters y setters
  /// getters
  obj->getPacienteId  	= getPacienteId_Impl;
  obj->getNroLegajo    	= getNroLegajoPaciente_Impl;
  obj->getFechaNac  	= getFechaNacPaciente_Impl;
  obj->getDni  	  		= getDniPaciente_Impl;
  obj->getApellido 		= getApellidoPaciente_Impl;
  obj->getNombres 		= getNombresPaciente_Impl;
  obj->getDomicilio 	= getNombresPaciente_Impl;
  obj->getTelefono 		= getTelefonoPaciente_Impl;  
  obj->getCodObraSocial = getCodObraSocialPaciente_Impl;
  obj->getCodLocalidad 	= getCodLocalidadPaciente_Impl;  
  /// setters  
  obj->setPacienteId    = setPacienteId_Impl;
  obj->setNroLegajo     = setNroLegajoPaciente_Impl;
  obj->setFechaNac 		= setFechaNacPaciente_Impl;
  obj->setDni     		= setDniPaciente_Impl;
  obj->setApellido 		= setApellidoPaciente_Impl;
  obj->setNombres 		= setNombresPaciente_Impl;
  obj->setDomicilio 	= setDomicilioPaciente_Impl;
  obj->setTelefono 		= setTelefonoPaciente_Impl;
  obj->setCodObraSocial = setCodObraSocialPaciente_Impl;
  obj->setCodLocalidad  = setCodLocalidadPaciente_Impl;
  //incializacion de la interfaz de la entidad
  INIT_COMMON
  
  obj->toString    		= toString_PacienteImpl;
  // implementar detroy internal para liberar recursos  
  obj->destroyInternal 	= destroyInternalPac_Impl;
  //---- acceso a relaciones  
  obj->getLocalidadObj 	= getLocalidadPacienteObj_Impl;
  obj->getObraSocialObj = getObraSocialPacienteObj_Impl;
  return obj;
}
//----------------------------------------------------
//constructor de Paciente
obj_Paciente *Paciente_new()
{
  return (obj_Paciente *)init_obj(sizeof(obj_Paciente), init_Paciente);
}
//----------------------------------------------------
