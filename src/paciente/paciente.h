#ifndef PAC_INCLUDED
	#define PAC_INCLUDED
	#define CNT_COL_PAC 10
	#define POS_ID 0
	#define POS_NROLEGAJO_PAC 1
	#define POS_FNAC_PAC 2
	#define POS_DNI_PAC 3
	#define POS_APELLIDO_PAC 4
	#define POS_NOMBRES_PAC 5
	#define POS_DOMICILIO_PAC 6
	#define POS_TELEFONO_PAC 7
	#define POS_COD_OS_PAC 8
	#define POS_COD_LOC_PAC 9
	//----------------------------------------------------
	typedef struct {
	    TYPEDEFCOMMON//// FIN CORRESPONDENCIA COMUN CON t_object
	    //-- getters
	    getPropertyIntPtr   getPacienteId;
	    getPropertyIntPtr   getNroLegajo;	    
		getPropertyCharPtr  getFechaNac;
		getPropertyIntPtr   getDni;
		getPropertyCharPtr  getApellido;
		getPropertyCharPtr  getNombres;
		getPropertyCharPtr  getDomicilio;
		getPropertyCharPtr  getTelefono;
		getPropertyIntPtr   getCodObraSocial;
		getPropertyIntPtr   getCodLocalidad;
	    //-- setters
	    setPropertyIntPtr   setPacienteId;
	    setPropertyIntPtr   setNroLegajo;
		setPropertyCharPtr  setFechaNac;
		setPropertyIntPtr   setDni;
		setPropertyCharPtr  setApellido;
		setPropertyCharPtr  setNombres;
		setPropertyCharPtr  setDomicilio;
		setPropertyCharPtr  setTelefono;
		setPropertyIntPtr   setCodObraSocial;
		setPropertyIntPtr   setCodLocalidad;
		getLocalidadObjPtr  getLocalidadObj;
		getObraSocialObjPtr getObraSocialObj;
	    
	    obj_Localidad *localidad;
	    obj_ObraSocial *obrasoc;
	    //void *localidad;
	}obj_Paciente;
	// funcionalidad publica que se implementa en Paciente.c
	extern obj_Paciente *Paciente_new ();
	// meta data para acceder a Pacientes - definicion de las columnas de la tabla
	static t_column cols_Paciente[CNT_COL_PAC]=
	{ 
	//definicion del mapeo, por cada columna de la tabla con los atributos del pseudobjeto.
	// nombre de la columna, tipo, tama�o, si es clave, si es autonumerado.
	{"id",t_int,sizeof(int),true,true}, 
	{"nro_legajo",t_int,sizeof(int),false,false},
	{"fecha_nac",t_date,(sizeof(char)*MAXFECHA)+1,false,false},
	{"dni",t_int,sizeof(int),false,false},
	{"apellido",t_varchar,(sizeof(char)*MAX50)+1,false,false},
	{"nombres",t_varchar,(sizeof(char)*MAX90)+1,false,false},
	{"domicilio",t_varchar,(sizeof(char)*MAX)+1,false,false},
	{"telefono",t_varchar,(sizeof(char)*MAX20)+1,false,false},
	{"cod_obra_social",t_int,sizeof(int),false,false},
	{"cod_localidad",t_int,sizeof(int),false,false},
	};
	// plantilla para la Paciente.
	static t_table table_Paciente={"paciente",CNT_COL_PAC,0, cols_Paciente,NULL};
	typedef obj_Paciente *(*getPacienteObjPtr)(void *self);
#endif
