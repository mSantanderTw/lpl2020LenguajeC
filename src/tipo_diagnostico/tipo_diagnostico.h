#ifndef TIPO_DIAG_INCLUDED
#define TIPO_DIAG_INCLUDED
	#define CNT_COL_TIPODIAG 2
	#define POS_ID 0
	#define POS_DESCRIPCION 1
	//----------------------------------------------------
	typedef struct {
	    TYPEDEFCOMMON//// FIN CORRESPONDENCIA COMUN CON t_object
	    //-- getters
	    getPropertyIntPtr getTipoDiagnosticoId;
		getPropertyCharPtr getDescripcion;	    
	    //-- setters
	    setPropertyIntPtr setTipoDiagnosticoId;
		setPropertyCharPtr setDescripcion;  
		
	}obj_TipoDiagnostico;
	// funcionalidad publica que se implementa en TipoDiagnostico.c
	extern obj_TipoDiagnostico *TipoDiagnostico_new ();
	// meta data para acceder a TipoDiagnosticos - definicion de las columnas de la tabla
	static t_column cols_TipoDiagnostico[CNT_COL_TIPODIAG]=
	{ 
	//definicion del mapeo, por cada columna de la tabla con los atributos del pseudobjeto.
	// nombre de la columna, tipo, tama�o, si es clave, si es autonumerado.
	{"id",t_int,sizeof(int),true,true}, 
	{"descripcion",t_varchar,(sizeof(char)*MAX)+1,false,false}
	};
	// plantilla para la TipoDiagnostico.
	static t_table table_TipoDiagnostico={"tipo_diagnostico",CNT_COL_TIPODIAG,0, cols_TipoDiagnostico,NULL};
	typedef obj_TipoDiagnostico *(*getTipoDiagnosticoObjPtr)(void *self);
#endif
