#include "../../includes/includelib.h"
#include "tipo_diagnostico.h"

THIS(obj_TipoDiagnostico)// crea definicion de funcion this para este modulo. .. Macro en config.h
//----------------------------------------------------
static void toString_TipoDiagnosticoImpl(void *self)
{
     obj_TipoDiagnostico *obj=this(self);
     printf("id: %d  Descripcion:%s \n",
	 obj->getTipoDiagnosticoId(obj),
	 obj->getDescripcion(obj)
	 );
}
//----------------------------------------------------
//implementacion de getters
//----------------------------------------------------
static int getTipoDiagnosticoId_Impl(void *self)
{
  RET_INT(POS_ID)//return *( (int *) getValue(self,POS_ID));
}
//----------------------------------------------------
static char *getDescripcionTipoDiagnostico_Impl(void *self)
{
  RET_CAD(POS_DESCRIPCION)//return  (char *) getValue(self,POS_DESCRIPCION);
}
//----------------------------------------------------
//implementacion setters
//----------------------------------------------------
static void setTipoDiagnosticoId_Impl(void *self,int val)
{ 
	setValue(self,POS_ID,&val);
}
//----------------------------------------------------
static void setDescripcionTipoDiagnostico_Impl(void *self,char *descripcion_TipoDiagnostico)
{ 
	setValue(self,POS_DESCRIPCION,descripcion_TipoDiagnostico);
}
//----------------------------------------------------
//implementacion de relaciones
//----------------------------------------------------
/// ....
//----------------------------------------------------
//implementacion constructor
//----------------------------------------------------
static void *init_TipoDiagnostico(void *self)
{
  obj_TipoDiagnostico *obj  = this(self);
  obj->ds  					= &table_TipoDiagnostico;
  obj->constructor 			= TipoDiagnostico_new;
  obj->sizeObj 				= sizeof(obj_TipoDiagnostico*);
  // Inicializar handlers de getters y setters
  /// getters
  obj->getTipoDiagnosticoId = getTipoDiagnosticoId_Impl;
  obj->getDescripcion		= getDescripcionTipoDiagnostico_Impl;  
  /// setters  
  obj->setTipoDiagnosticoId = setTipoDiagnosticoId_Impl;
  obj->setDescripcion		= setDescripcionTipoDiagnostico_Impl;
  //incializacion de la interfaz de la entidad
  INIT_COMMON
  
  obj->toString    			= toString_TipoDiagnosticoImpl;
  return obj;
}
//----------------------------------------------------
//constructor de TipoDiagnostico
obj_TipoDiagnostico *TipoDiagnostico_new()
{
  return (obj_TipoDiagnostico *)init_obj(sizeof(obj_TipoDiagnostico), init_TipoDiagnostico);
}
//----------------------------------------------------
