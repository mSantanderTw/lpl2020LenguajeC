#include "../../includes/includelib.h"
#include "consultorio.h"

THIS(obj_Consultorio)// crea definicion de funcion this para este modulo. .. Macro en config.h
//----------------------------------------------------
static void toString_ConsultorioImpl(void *self)
{
     obj_Consultorio *obj=this(self);     
     printf("Consultorio_id: %d\tConsultorio:%s \tDomicilio: %s\tTelefono: %s\n",
	 obj->getConsultorioId(obj),
	 obj->getDenominacion(obj),
	 obj->getDomicilio(obj),
	 obj->getTelefono(obj)
	 );
}
//----------------------------------------------------
//implementacion de getters
//----------------------------------------------------
static int getConsultorioId_Impl(void *self)
{
  RET_INT(POS_ID)
}
//----------------------------------------------------
static char *getDenominacionConsultorio_Impl(void *self)
{
  RET_CAD(POS_DENOMINACION)
}
//----------------------------------------------------
static char *getDomicilioConsultorio_Impl(void *self)
{
  RET_CAD(POS_DOMICILIO)
}
//----------------------------------------------------
static char *getTelefonoConsultorio_Impl(void *self)
{
  RET_CAD(POS_TELEFONO)
}
//----------------------------------------------------
//implementacion setters
//----------------------------------------------------
static void setConsultorioId_Impl(void *self,int val)
{ 
	setValue(self,POS_ID,&val);
}
//----------------------------------------------------
static void setDenominacionConsultorio_Impl(void *self,char *denominacion)
{ 
	setValue(self,POS_DENOMINACION,denominacion);
}
//----------------------------------------------------
static void setDomicilioConsultorio_Impl(void *self,char *domicilio)
{ 
	setValue(self,POS_DOMICILIO,domicilio);
}
//----------------------------------------------------
static void setTelefonoConsultorio_Impl(void *self,char *telefono)
{ 
	setValue(self,POS_TELEFONO,telefono);
}
//----------------------------------------------------
//implementacion de relaciones
//----------------------------------------------------
/// ....
//----------------------------------------------------
//implementacion constructor
//----------------------------------------------------
static void *init_Consultorio(void *self)
{
  obj_Consultorio *obj  = this(self);
  obj->ds               = &table_Consultorio;
  obj->constructor      = Consultorio_new;
  obj->sizeObj          = sizeof(obj_Consultorio*);
  // Inicializar handlers de getters y setters
  /// getters
  obj->getConsultorioId = getConsultorioId_Impl;
  obj->getDenominacion  = getDenominacionConsultorio_Impl;
  obj->getDomicilio     = getDomicilioConsultorio_Impl;
  obj->getTelefono      = getTelefonoConsultorio_Impl;  
  /// setters  
  obj->setConsultorioId = setConsultorioId_Impl;
  obj->setDenominacion  = setDenominacionConsultorio_Impl;
  obj->setDomicilio     = setDomicilioConsultorio_Impl;
  obj->setTelefono      = setTelefonoConsultorio_Impl;
  //incializacion de la interfaz de la entidad
  INIT_COMMON
  obj->toString         = toString_ConsultorioImpl;
  return obj;
}
//----------------------------------------------------
//constructor de Consultorio
obj_Consultorio *Consultorio_new()
{
  return (obj_Consultorio *)init_obj(sizeof(obj_Consultorio), init_Consultorio);
}
//----------------------------------------------------
