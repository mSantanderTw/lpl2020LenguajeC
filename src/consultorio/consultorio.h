#ifndef CONS_INCLUDED
	#define CONS_INCLUDED
	#define CNT_COL_CONS 4
	#define POS_ID 0
	#define POS_DENOMINACION 1
	#define POS_DOMICILIO 2
	#define POS_TELEFONO 3
	//----------------------------------------------------
	typedef struct {
		TYPEDEFCOMMON//// FIN CORRESPONDENCIA COMUN CON t_object
		//-- getters
		getPropertyIntPtr  getConsultorioId;
		getPropertyCharPtr getDenominacion;
		getPropertyCharPtr getDomicilio;
		getPropertyCharPtr getTelefono;	
		//-- setters
		setPropertyIntPtr  setConsultorioId;
		setPropertyCharPtr setDenominacion;
		setPropertyCharPtr setDomicilio;
		setPropertyCharPtr setTelefono;	
	}obj_Consultorio;
	// funcionalidad publica que se implementa en Consultorio.c
	extern obj_Consultorio *Consultorio_new ();
	// meta data para acceder a  Consultorios - definicion de las columnas de la tabla
	static t_column cols_Consultorio[CNT_COL_CONS]=
	{ 
	//definicion del mapeo, por cada columna de la tabla con los atributos del pseudobjeto.
	// nombre de la columna, tipo, tama�o, si es clave, si es autonumerado.
	{"id",t_int,sizeof(int),true,true}, 
	{"denominacion",t_varchar,(sizeof(char)*MAX40)+1,false,false},
	{"domicilio",t_varchar,(sizeof(char)*MAX90)+1,false,false},
	{"telefono",t_varchar,(sizeof(char)*MAX20)+1,false,false}
	};
	// plantilla para la Consultorio.
	static t_table table_Consultorio={"consultorio",CNT_COL_CONS,0, cols_Consultorio,NULL};
	typedef obj_Consultorio *(*getConsultorioObjPtr)(void *self);
#endif
