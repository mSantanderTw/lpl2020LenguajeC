#include "../../includes/includelib.h"
#include "../localidad/localidad.h"
#include "../obra_social/obra_social.h"
#include "../consultorio/consultorio.h"
#include "../paciente/paciente.h"
#include "../profesional/profesional.h"
#include "turno.h"

THIS(obj_Turno)// crea definicion de funcion this para este modulo. .. Macro en config.h
//----------------------------------------------------
static void toString_TurnoImpl(void *self)
{
     obj_Turno *obj=this(self);
     obj_Consultorio *cons = obj->getConsultorioObj(obj);
	 obj_Paciente *pac = obj->getPacienteObj(obj);
     obj_Profesional *prof = obj->getProfesionalObj(obj);
          
     printf("Turno_id: %d - \tPac:%s,%s\t - Prof: %s,%s \tConsultorio:%s - Fecha:%s\n",
	 obj->getTurnoId(obj),
	 pac->getApellido(pac),
	 pac->getNombres(pac),
	 prof->getApellido(prof),
	 prof->getNombres(prof),
	 cons->getDenominacion(cons),
	 obj->getFecha(obj)
	 );
}
//----------------------------------------------------
//implementacion de getters
//----------------------------------------------------
static int getTurnoId_Impl(void *self)
{
  RET_INT(POS_ID)
}
//----------------------------------------------------
static int getCodConsultorio_T_Impl(void *self)
{
  RET_INT(POS_COD_CONSULTORIO_T)
}
//----------------------------------------------------
static int getCodPaciente_T_Impl(void *self)
{
  RET_INT(POS_COD_PACIENTE_T)
}
//----------------------------------------------------
static int getCodProfesional_T_Impl(void *self)
{
  RET_INT(POS_COD_PROFESIONAL_T)
}
//----------------------------------------------------
static char *getFecha_T_Impl(void *self)
{
  RET_CAD(POS_FECHA_T)
}
//----------------------------------------------------
//implementacion setters
//----------------------------------------------------
static void setTurnoId_Impl(void *self,int val)
{ 
	setValue(self,POS_ID,&val);
}
//----------------------------------------------------
static void setCodConsultorio_T_Impl(void *self,int val)
{ 
	setValue(self,POS_COD_CONSULTORIO_T,&val);
}
//----------------------------------------------------
static void setCodPaciente_T_Impl(void *self,int val)
{ 
	setValue(self,POS_COD_PACIENTE_T,&val);
}
//----------------------------------------------------
static void setCodProfesional_T_Impl(void *self,int val)
{ 
	setValue(self,POS_COD_PROFESIONAL_T,&val);
}
//----------------------------------------------------
static void setFecha_T_Impl(void *self,char *fecha_desde)
{ 
	setValue(self,POS_FECHA_T,fecha_desde);
}
//----------------------------------------------------
static void destroyInternalTurno_Impl(void *self)
{
	obj_Turno *obj = this(self);
	
	if(obj->paciente!=NULL)
	  destroyObj(obj->paciente);
	if(obj->profesional!=NULL)
	  destroyObj(obj->profesional);
	if(obj->consultorio!=NULL)
	  destroyObj(obj->consultorio);
}
//----------------------------------------------------
//implementacion de relaciones
//----------------------------------------------------
/// ....
obj_Paciente *getPacienteTurnoObj_Impl(void *self)
{
	///IMPLEMENTAR....
	return NULL;
}
//----------------------------------------------------
obj_Profesional *getProfesionalTurnoObj_Impl(void *self)
{
	///IMPLEMENTAR....
	return NULL;
}
//----------------------------------------------------
obj_Consultorio *getConsultorioTurnoObj_Impl(void *self)
{
	///IMPLEMENTAR....
	return NULL;
}
//----------------------------------------------------
//implementacion constructor
//----------------------------------------------------
static void *init_Turno(void *self)
{
  obj_Turno *obj  = this(self);
  obj->ds                     = &table_Turno;
  obj->constructor            = Turno_new;
  obj->sizeObj                = sizeof(obj_Turno*);
  obj->paciente				  = NULL;
  obj->profesional		  = NULL;
  // Inicializar handlers de getters y setters
  /// getters
  obj->getTurnoId = getTurnoId_Impl;
  obj->getCodConsultorio         = getCodConsultorio_T_Impl;
  obj->getCodPaciente         = getCodPaciente_T_Impl;
  obj->getCodProfesional  = getCodProfesional_T_Impl;
  obj->getFecha               = getFecha_T_Impl;  
  /// setters  
  obj->setTurnoId = setTurnoId_Impl;
  obj->setCodConsultorio         = setCodConsultorio_T_Impl;
  obj->setCodPaciente         = setCodPaciente_T_Impl;
  obj->setCodProfesional  = setCodProfesional_T_Impl;
  obj->setFecha               = setFecha_T_Impl;
  //incializacion de la interfaz de la entidad
  INIT_COMMON

  obj->toString               = toString_TurnoImpl;
  // implementar detroy internal para liberar recursos
  obj->destroyInternal        = destroyInternalTurno_Impl;
  //---- acceso a relaciones    
  obj->getConsultorioObj		  = getConsultorioTurnoObj_Impl;
  obj->getPacienteObj		  = getPacienteTurnoObj_Impl;
  obj->getProfesionalObj  = getProfesionalTurnoObj_Impl;
  return obj;
}
//----------------------------------------------------
//constructor de Turno
obj_Turno *Turno_new()
{
  return (obj_Turno *)init_obj(sizeof(obj_Turno), init_Turno);
}
//----------------------------------------------------
