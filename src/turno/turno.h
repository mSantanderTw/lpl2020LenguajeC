#ifndef TURNO_INCLUDED
	#define TURNO_INCLUDED
	#define CNT_COL_TURNO 6
	#define POS_ID 0
	#define POS_COD_CONSULTORIO_T 1
	#define POS_COD_PROFESIONAL_T 2
	#define POS_COD_PACIENTE_T 3
	#define POS_ESTADO_T 4
	#define POS_FECHA_T 5
	
	//----------------------------------------------------
	typedef struct {
	    TYPEDEFCOMMON//// FIN CORRESPONDENCIA COMUN CON t_object
	    //-- getters
	    getPropertyIntPtr    getTurnoId;
	    getPropertyIntPtr    getCodPaciente;
	    getPropertyIntPtr    getCodConsultorio;
	    getPropertyIntPtr    getCodProfesional;
	    getPropertyCharPtr   getFecha;	    
	    //-- setters
	    setPropertyIntPtr    setTurnoId;
	    setPropertyIntPtr    setCodConsultorio;
	    setPropertyIntPtr    setCodPaciente;
	    setPropertyIntPtr    setCodProfesional;
	    setPropertyCharPtr   setFecha;
	    getPacienteObjPtr    getPacienteObj;
	    getProfesionalObjPtr getProfesionalObj;
	    getConsultorioObjPtr getConsultorioObj;
		
	    obj_Paciente *paciente;
		obj_Profesional *profesional;
		obj_Consultorio *consultorio;		
	}obj_Turno;
	// funcionalidad publica que se implementa en Turno.c
	extern obj_Turno *Turno_new();
	// meta data para acceder a Turnos - definicion de las columnas de la tabla
	static t_column cols_Turno[CNT_COL_TURNO]=
	{ 
	//definicion del mapeo, por cada columna de la tabla con los atributos del pseudobjeto.
	// nombre de la columna, tipo, tama�o, si es clave, si es autonumerado.
	{"id",t_int,sizeof(int),true,true}, 
	{"cod_consultorio",t_int,sizeof(int),false,false},
	{"cod_profesional",t_int,sizeof(int),false,false},
	{"cod_paciente",t_int,sizeof(int),false,false},
	{"estado",t_int,sizeof(int),false,false},
	{"fecha",t_varchar,(sizeof(char)*MAX10)+1,false,false},
	};
	// plantilla para la Turno.
	static t_table table_Turno={"turno",CNT_COL_TURNO,0, cols_Turno,NULL};
#endif
