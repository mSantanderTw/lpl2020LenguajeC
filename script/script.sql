CREATE TABLE localidad
(
  id int primary key, --CP
  nombre varchar(50)
);

CREATE TABLE obra_social
(
  id integer primary key,
  descripcion varchar(40),
  domicilio_principal varchar(120)
);

CREATE TABLE tipo_diagnostico
(
  id int primary key,
  descripcion varchar(30)
);

CREATE TABLE paciente
(
  id serial primary key,
  nro_legajo integer,
  fecha_nac date,
  dni integer,
  apellido varchar(50),
  nombres varchar(90),
  domicilio varchar(80),
  telefono varchar(20),
  cod_obra_social integer,
  cod_localidad int
);
alter table paciente add constraint fk_paciente_obrasocial 
foreign key(cod_obra_social) references obra_social(id);

alter table paciente add constraint fk_paciente_localidad
foreign key(cod_localidad) references localidad(id);


CREATE TABLE historial_paciente
(
 id int primary key,
 cod_paciente int,
 cod_diagnostico int, 
 fecha TIMESTAMP WITH TIME ZONE,
 indicaciones varchar(120),
 observaciones varchar(150)
);

alter table historial_paciente add constraint fk_historialpaciente_diagnostico 
foreign key(cod_diagnostico) references tipo_diagnostico(id);

alter table historial_paciente add constraint fk_historialpaciente_paciente 
foreign key(cod_paciente) references paciente(id);

CREATE TABLE especialidad
(
 id int primary key,
 descripcion varchar(50)
);

CREATE TABLE profesional
(
 id serial primary key,
 matricula varchar(20),
 dni int,
 apellido varchar(50),
 nombres varchar(90),
 domicilio varchar(80),
 cod_localidad int,
 telefono varchar(20),
 disponible smallint
);

alter table profesional add constraint fk_profesional_localidad
foreign key(cod_localidad) references localidad(id);

CREATE TABLE profesional_especialidad
(
 id serial primary key,
 cod_profesional int,
 cod_especialidad int,
 fecha_desde date 
);
alter table profesional_especialidad add constraint fk_profesional_especialidad_profesional foreign key(cod_profesional) references profesional(id);

alter table profesional_especialidad add constraint fk_profesional_especialidad_especialidad foreign key(cod_especialidad) references especialidad(id);

CREATE TABLE consultorio
(
  id serial primary key,
  denominacion varchar(40),
  domicilio varchar(90),
  telefono varchar(20),
  disponible smallint
);

CREATE TABLE turno
(
  id serial primary key,
  cod_consultorio int,
  cod_profesional int,
  cod_paciente int,
  estado smallint, --1:pendiente - 2:realizado 3:suspendido
  fecha TIMESTAMP WITH TIME ZONE
);

alter table turno add constraint fk_turno_consultorio foreign key(cod_consultorio) references consultorio(id);

alter table turno add constraint fk_turno_profesional foreign key(cod_profesional) references profesional(id);

alter table turno add constraint fk_turno_paciente    foreign key(cod_paciente) references    paciente(id);

insert into obra_social(id, descripcion, domicilio_principal)
values
(1,'SEROS','Rawson - 25 de mayo 200'),
(2,'DASU','CR - mitre 122'),
(3,'OSDE','Trelew - Pellegrini 50'),
(4,'OSPRERA','Trelew - Rawson 435'),
(5,'ADOS','Trelew - Moreno 125');

insert into tipo_diagnostico(id, descripcion)
values(1,'Gripe'),(2,'Fractura'),(3,'Gastroenteritis'),(4,'Laringitis'),(5,'Anginas'),(6,'Covid-19'),(7,'Dengue'),(8,'hipertension'),(9,'diabetes');

insert into especialidad(id,descripcion) values
(1, 'Medicina General'),(2,'Ginecologia'),(3,'Traumatologia'),(4,'Neurologia'),(5,'Cardiologia'),(6,'Alergista');

insert into localidad (id,nombre) 
values(1,'Trelew'),(2,'Rawson'),(3,'Gaiman'),(4,'Puerto Madryn'),(5,'Comodoro Rivadavia'),(6,'Esquel');

insert into consultorio( id, denominacion, domicilio, telefono)
values(1,'Clinica del Valle','Pellegrini 652','445666'),(2,'Sanatorio Trelew','Pecoraro 237','443454'),(3,'Instituto del Sur','Moreno 135','445678'),(4,'San Miguel','Edison 148','4412345'),(5,'Clinica Favaloro','Marconi 45','441221');

insert into paciente(id, nro_legajo,fecha_nac,dni,apellido,nombres,domicilio,telefono,cod_obra_social,cod_localidad)
values(1,150,'1972-04-26',22345678,'Lopez','Eduardo Ruben','mitre 233','15434567',1,1);

insert into profesional(id,matricula,dni,apellido,nombres,domicilio,telefono,cod_localidad)
values(1,'m567',20345768,'Rodriguez','Ariel','Ecuador 345','15476980',1);


insert into profesional_especialidad(id,cod_profesional,cod_especialidad,fecha_desde)
values(1,1,1,'2010-05-01');

insert into historial_paciente(id, cod_paciente, cod_diagnostico,fecha,indicaciones, observaciones)
values(1,1,1,'2020-01-09','reposo','afeccion leve');

insert into turno(id,cod_consultorio,cod_profesional,cod_paciente,estado, fecha)
values(1,1,1,1,1,'2020-03-04');